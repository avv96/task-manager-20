package ru.tsc.vinokurov.tm.repository;

import ru.tsc.vinokurov.tm.api.repository.IUserOwnedRepository;
import ru.tsc.vinokurov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @Override
    public List<M> findAll(final String userId) {
        final List<M> result = items.stream()
                .filter(item -> userId.equals(item.getUserId()))
                .collect(Collectors.toList());
        return result;
    }

    @Override
    public List<M> findAll(final String userId, final Comparator comparator) {
        final List<M> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    @Override
    public M add(final String userId, final M item) {
        item.setUserId(userId);
        return add(item);
    }

    @Override
    public M remove(final String userId, final M item) {
        if (userId != item.getUserId()) return null;
        return remove(item);
    }

    @Override
    public void clear(final String userId) {
        final List<M> items = findAll(userId);
        removeAll(items);
    }

    @Override
    public M findOneByIndex(final String userId, final Integer index) {
        return findAll(userId).get(index);
    }

    @Override
    public M findOneById(final String userId, final String id) {
        final M result = items.stream()
                .filter(item -> userId.equals(item.getUserId()) && id.equals(item.getId()))
                .findFirst().orElse(null);
        return result;
    }

    @Override
    public int size(final String userId) {
        return findAll(userId).size();
    }

}
