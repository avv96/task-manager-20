package ru.tsc.vinokurov.tm.repository;

import ru.tsc.vinokurov.tm.api.repository.IRepository;
import ru.tsc.vinokurov.tm.model.AbstractModel;
import ru.tsc.vinokurov.tm.model.Project;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

//
public class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    protected final List<M> items = new ArrayList<>();

    @Override
    public List<M> findAll() {
        return items;
    }

    @Override
    public List<M> findAll(final Comparator comparator) {
        final List<M> result = new ArrayList<>(items);
        result.sort(comparator);
        return result;
    }

    @Override
    public M add(final M item) {
        items.add(item);
        return item;
    }

    @Override
    public M remove(final M item) {
        items.remove(item);
        return item;
    }

    @Override
    public void removeAll(final Collection<M> collection) {
        items.removeAll(collection);
    }

    @Override
    public void clear() {
        items.clear();
    }

    @Override
    public M findOneByIndex(final Integer index) {
        return items.get(index);
    }

    @Override
    public M findOneById(final String id) {
        return items.stream().filter(project -> id.equals(project.getId())).findAny().orElse(null);
    }

    @Override
    public M removeById(String id) {
        final M item = findOneById(id);
        if (item == null) return null;
        items.remove(item);
        return item;
    }

    @Override
    public M removeByIndex(Integer index) {
        final M item = findOneByIndex(index);
        if (item == null) return null;
        items.remove(item);
        return item;
    }

    @Override
    public int size() {
        return items.size();
    }

}
