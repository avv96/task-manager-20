package ru.tsc.vinokurov.tm.api.repository;

import ru.tsc.vinokurov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task>{

    Task create(String userId, String name);

    Task create(String userId, String name, String description);

    List<Task> findAllByProjectId(String userId, String projectId);

    boolean existsById(String userId, String id);

}
