package ru.tsc.vinokurov.tm.api.service;

import ru.tsc.vinokurov.tm.enumerated.Sort;
import ru.tsc.vinokurov.tm.enumerated.Status;
import ru.tsc.vinokurov.tm.model.Project;
import ru.tsc.vinokurov.tm.model.Task;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public interface IProjectService extends IUserOwnedService<Project>{

    Project create(String userId, String name);

    Project create(String userId, String name, String description);

    Project create(String userId, String name, String description, Date dateBegin, Date dateEnd);

    Project updateById(String userId, String id, String name, String description);

    Project updateByIndex(String userId, Integer index, String name, String description);

    Project changeStatusById(String userId, String id, Status status);

    Project changeStatusByIndex(String userId, Integer index, Status status);

}
