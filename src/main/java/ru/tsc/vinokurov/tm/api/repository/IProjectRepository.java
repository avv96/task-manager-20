package ru.tsc.vinokurov.tm.api.repository;

import ru.tsc.vinokurov.tm.model.Project;
import ru.tsc.vinokurov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends  IUserOwnedRepository<Project>{

    Project create(String userId, String name);

    Project create(String userId, String name, String description);

    boolean existsById(String userId, String id);

}
