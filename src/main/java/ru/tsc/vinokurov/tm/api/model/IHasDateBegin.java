package ru.tsc.vinokurov.tm.api.model;

import java.util.Date;

public interface IHasDateBegin {

    Date getDateBegin();

    void setDateBegin(Date dateBegin);

}
