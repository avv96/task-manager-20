package ru.tsc.vinokurov.tm.model;

public class AbstractUserOwnedModel extends AbstractModel {

    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(final String userId) {
        this.userId = userId;
    }

}
