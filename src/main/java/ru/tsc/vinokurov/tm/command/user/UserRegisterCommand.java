package ru.tsc.vinokurov.tm.command.user;

import ru.tsc.vinokurov.tm.enumerated.Role;
import ru.tsc.vinokurov.tm.util.TerminalUtil;

public final class UserRegisterCommand extends AbstractUserCommand {

    public static final String NAME = "user-register";

    public static final String DESCRIPTION = "Register new user.";

    public static final String ARGUMENT = null;

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[REGISTER NEW USER]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        final String email = TerminalUtil.nextLine();
        getUserService().create(login, password, email);
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
