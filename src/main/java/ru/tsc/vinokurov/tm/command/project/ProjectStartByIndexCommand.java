package ru.tsc.vinokurov.tm.command.project;

import ru.tsc.vinokurov.tm.enumerated.Status;
import ru.tsc.vinokurov.tm.util.TerminalUtil;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    public static final String NAME = "project-start-by-index";

    public static final String DESCRIPTION = "Start project by index.";

    public static final String ARGUMENT = null;

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = getUserId();
        getProjectService().changeStatusByIndex(userId, index, Status.IN_PROGRESS);
    }

}
