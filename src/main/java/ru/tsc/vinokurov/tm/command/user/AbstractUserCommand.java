package ru.tsc.vinokurov.tm.command.user;

import ru.tsc.vinokurov.tm.api.service.IAuthService;
import ru.tsc.vinokurov.tm.api.service.IUserService;
import ru.tsc.vinokurov.tm.command.AbstractCommand;

public abstract class AbstractUserCommand extends AbstractCommand {

    protected IUserService getUserService() {
        return serviceLocator.getUserService();
    }

}
