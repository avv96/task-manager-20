package ru.tsc.vinokurov.tm.command.user;

import ru.tsc.vinokurov.tm.enumerated.Role;

public final class UserLogoutCommand extends AbstractUserCommand {

    public static final String NAME = "user-logout";

    public static final String DESCRIPTION = "Logout user.";

    public static final String ARGUMENT = null;

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        getAuthService().logout();
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
