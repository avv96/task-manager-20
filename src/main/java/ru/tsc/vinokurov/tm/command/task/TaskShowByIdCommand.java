package ru.tsc.vinokurov.tm.command.task;

import ru.tsc.vinokurov.tm.model.Task;
import ru.tsc.vinokurov.tm.util.TerminalUtil;

public final class TaskShowByIdCommand extends AbstractTaskCommand {

    public static final String NAME = "task-show-by-id";

    public static final String DESCRIPTION = "Show task by id.";

    public static final String ARGUMENT = null;

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final String userId = getUserId();
        final Task task = getTaskService().findOneById(userId, id);
        showTask(task);
    }


}
