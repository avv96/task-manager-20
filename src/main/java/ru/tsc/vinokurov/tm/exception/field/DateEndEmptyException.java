package ru.tsc.vinokurov.tm.exception.field;

public final class DateEndEmptyException extends AbstractFieldException {
    public DateEndEmptyException() {
        super("Error! Date End is empty...");
    }
}
